import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';
import Header from '../utilities/Header';
import HeaderTemp from '../utilities/Header1';

const { width, height } = Dimensions.get('window')

export default class MyCart extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
            showTotal: false,
            cartNo: 0,
            showHeader: false
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/order/cart", {}, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    if (result.cart_items.length > 0) {
                        this.setState({ showTotal: true })
                        Constants.cartCount = result.cart_items.length;
                        this.setState({ cartNo: result.cart_items.length, showHeader: true })
                    }

                    this.setState({ newOrders: result.cart_items })
                } else if (result.success == "0") {
                    alert(result.message)
                    this.props.navigation.goBack();
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    delete(productId) {
        RestClient.delete("api/customer/order/cart/remove", { product_id: productId }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                this.getNewDetails();
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    minus(productId, key, quantity) {
        if (quantity > 1) {
            RestClient.putQuery("api/customer/order/cart/decrease", { product_id: productId }, this.state.tokenValue).then((result) => {
                console.log("result", JSON.stringify(result))
                if (result.success == "1") {
                    this.getNewDetails();
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        } else {
            alert("Product quantity should be at-least one")
        }
    }

    plus(productId, key, quantity) {
        RestClient.putQuery("api/customer/order/cart/increase", { product_id: productId }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                this.getNewDetails();
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        let totalItems = 0, totalAmount = 0
        for (let i = 0; i < this.state.newOrders.length; i++) {
            totalItems = totalItems + this.state.newOrders[i].quantity;
            totalAmount = totalAmount + (this.state.newOrders[i].quantity * this.state.newOrders[i].discounted_price);
        }
        return (
            <SafeAreaView style={styles.containerSafe}>
                <Background style={styles.container}>
                    {this.state.showHeader ?
                        <Header
                            headerText="MyCart"
                            onDrawerOpen={() =>
                                this.openDraw()
                            }
                            goBackIcon={true}
                            //cartCount={this.state.cartNo}
                            navigation={this.props.navigation}
                        /> :
                        <HeaderTemp
                            headerText="MyCart"
                            onDrawerOpen={() =>
                                this.openDraw()
                            }
                            goBackIcon={true}
                            //cartCount={this.state.cartNo}
                            navigation={this.props.navigation}
                        />
                    }
                    <ScrollView style={{ flex: .95 }}>
                        {this.state.newOrders && this.state.newOrders.length > 0 && this.state.newOrders.map((data, key) => {
                            return (
                                <TouchableOpacity key={key} style={[styles.productBox, { margin: 10, flexDirection: 'row', flex: 1, }]}>
                                    <View style={{ flex: .3 }}>
                                        <Image
                                            style={{ alignSelf: 'center', height: 50, width: 50 }}
                                            source={{ uri: `${Connection.getBaseUrl()}/${data.img}` }}>
                                        </Image>
                                    </View>
                                    <View style={{ flex: .4, flexDirection: 'column', justifyContent: 'space-between' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.DarkGrey }}>{data.product_name}</Text>
                                        {/* <Text style={{ fontSize: 18, color: Constants.Colors.LightGray }}>{'Unit'}</Text> */}
                                        {/* <Text style={{ fontSize: 16, color: Constants.Colors.HeaderGreen }}>{'₹ ' + data.price}</Text> */}
                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                            <Text style={{ fontSize: 14, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + data.price}</Text>
                                            <Text style={{ fontSize: 14, color: Constants.Colors.HeaderGreen }}>{'₹ ' + data.discounted_price}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: .3, }}>
                                        <View style={{ justifyContent: 'space-between' }}>
                                            <TouchableOpacity style={{ justifyContent: 'flex-end', alignSelf: 'flex-end' }} onPress={() => this.delete(data.product_id)}>
                                                <Icon name="trash-o" size={30} color="red" />
                                            </TouchableOpacity>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                                <TouchableOpacity onPress={() => this.minus(data.product_id, key, data.quantity)}>
                                                    <Icon name="minus-square" size={30} color="red" />
                                                </TouchableOpacity>
                                                <Text style={{ marginLeft: 5, marginRight: 5, alignSelf: 'center', fontSize: 18 }}>{data.quantity}</Text>
                                                <TouchableOpacity onPress={() => this.plus(data.product_id, key, data.quantity)}>
                                                    <Icon name="plus-square" size={30} color={Constants.Colors.HeaderGreen} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>
                    {this.state.showTotal ?
                        <View style={[styles.productBox, { padding: 0, height: 40, justifyContent: 'space-between', flexDirection: 'row', flex: .05, }]}>
                            <Text style={{ flex: .6, paddingLeft: 8, paddingTop: 8, paddingBottom: 8 }}>Total Item : ({totalItems})</Text>
                            <TouchableOpacity onPress={() => navigate('PlaceOrder')} style={{ paddingRight: 5, borderRadius: 40 / 2, height: 40, backgroundColor: Constants.Colors.HeaderGreen, flex: .4, flexDirection: 'row', justifyContent: 'flex-end', alignSelf: 'center' }}>
                                <Text style={{ fontSize: 18, alignSelf: 'center', marginRight: 10 }}>{'₹ ' + totalAmount}</Text>
                                <View style={{ justifyContent: 'center', height: 20, alignSelf: 'center' }} >
                                    <Text style={{ color: Constants.Colors.White, fontSize: 18, textAlign: 'center', alignSelf: 'center', justifyContent: 'center' }}>Order ></Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                    }
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 100,
        borderRadius: 10,
        padding: 8,
        margin: 10,
        backgroundColor: Constants.Colors.White,
        elevation: 3,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})