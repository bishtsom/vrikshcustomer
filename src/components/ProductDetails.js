import React, { Component } from 'react'
import { FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Header from '../utilities/Header';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';
import HeaderTemp from '../utilities/Header1';

const { width } = Dimensions.get('window')

export default class ProductDetails extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
            categorieList: [],
            itemCount: 0,
            showHeader: false
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })


    }

    openDraw() {
        this.props.navigation.openDrawer()
    }

    minus() {
        if (this.state.itemCount > 0) {
            this.setState({ itemCount: (this.state.itemCount - 1) })
        }
    }

    plus() {
        this.setState({ itemCount: (this.state.itemCount + 1) })
    }

    addToCart(available) {
        if (this.state.tokenValue == null || this.state.tokenValue == '') {
            alert('Please Signin to the application for purchasing this item.')
        } else {
            if (available) {
                if (this.state.itemCount > 0) {
                    RestClient.post("api/customer/order/cart/add", { product_id: this.props.navigation.getParam('productDetail').product_id, quantity: this.state.itemCount }, this.state.tokenValue).then((result) => {
                        console.log("result", JSON.stringify(result))
                        if (result.success == "1") {
                            Constants.cartCount = Constants.cartCount + 1;
                            this.setState({ showHeader: true })
                            alert(result.message)
                            this.props.navigation.goBack();
                        } else {
                            alert(result.message)
                        }
                    }).catch(error => {
                        alert(error)
                    });
                } else {
                    alert('Please add quantity of this item')
                }
            } else {
                alert('This item is not available')
            }
        }
    }

    render() {
        const { navigation } = this.props;
        const productDetail = navigation.getParam('productDetail');
        console.log("product detail:", productDetail)
        return (
            <SafeAreaView style={styles.containerSafe}>
                <Background style={styles.container}>
                    {this.state.showHeader ?
                        <Header
                            headerText="ProductDetails"
                            onDrawerOpen={() => this.openDraw()}
                            goBackIcon={true}
                            navigation={this.props.navigation}
                        />
                        :
                        <HeaderTemp
                            headerText="ProductDetails"
                            onDrawerOpen={() => this.openDraw()}
                            goBackIcon={true}
                            navigation={this.props.navigation}
                        />
                    }
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView behavior={'position'}>
                            <View style={{ backgroundColor: Constants.Colors.White }}>
                                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                                    <View style={{ flex: .8, justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <View style={{ width: width, height: width / 1.5, backgroundColor: 'white', justifyContent: 'center' }}>
                                            <Image
                                                style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center' }}
                                                source={{ uri: `${Connection.getBaseUrl()}/${productDetail.prof_img}` }}>
                                            </Image>
                                        </View>
                                        <View style={{ margin: 20 }}>
                                            <Text style={{ fontWeight: '700', fontSize: 20, color: Constants.Colors.Black }}>{productDetail.product_name}</Text>
                                            <Text style={{ marginTop: 5, fontSize: 14, color: Constants.Colors.BlurGrey }} ellipsizeMode='tail'>{productDetail.description}</Text>
                                            {productDetail.available ?
                                                null
                                                :
                                                <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'flex-start', marginTop: 5 }]}>Not Available</Text>
                                            }
                                            <View style={{ flexDirection: 'row', }}>
                                                <Text style={{ alignSelf: 'center', fontSize: 14, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + productDetail.price}</Text>
                                                <Text style={{ alignSelf: 'center', fontSize: 14, color: Constants.Colors.HeaderGreen }}>{'₹ ' + productDetail.discounted_price}</Text>
                                                <Text style={{ alignSelf: 'center', marginTop: 20, marginLeft: 10, fontSize: 16, color: Constants.Colors.LightGray }}>{productDetail.unit}</Text>
                                                {productDetail.ratings == '0' ?
                                                    null
                                                    :
                                                    <View style={{ alignSelf: 'center', height: 25, flexDirection: 'row', backgroundColor: Constants.Colors.HeaderGreen, justifyContent: 'center', marginLeft: 20, }}>
                                                        <Text style={{ paddingLeft: 5, alignSelf: 'center', fontSize: 14, color: Constants.Colors.White }}>{productDetail.ratings}</Text>
                                                        <Icon name="star" size={10} color="white" style={{ alignSelf: 'center', paddingRight: 5 }} />
                                                    </View>
                                                }
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                <TouchableOpacity onPress={() => this.minus()}>
                                                    <Icon name="minus-square" size={30} color="red" />
                                                </TouchableOpacity>
                                                <Text style={{ marginLeft: 30, marginRight: 30, alignSelf: 'center', fontSize: 18 }}>{this.state.itemCount}</Text>
                                                <TouchableOpacity onPress={() => this.plus()}>
                                                    <Icon name="plus-square" size={30} color={Constants.Colors.HeaderGreen} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    <View stysle={{ flex: .2, alignSelf: 'flex-end' }}>
                                        <SubmitButton
                                            onPress={() => this.addToCart(productDetail.available)}
                                            text="Add to Cart"
                                        />
                                    </View>
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})