import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import { scaleHeight, scaleWidth, normalizeFont } from "../utilities/Responsive";
import FormTextInput from '../common/FormTextInput';
import SubmitButton from "../common/FormSubmitButton";

const { width } = Dimensions.get('window')

export default class Feedback extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            feedback: ''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    sendFeedback() {
        RestClient.post("api/customer/feedback", {description: this.state.feedback}, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
                this.props.navigation.goBack();
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        <View style={{flex: 1, justifyContent:'space-between', marginTop: 30}}>
                            <FormTextInput
                                autoFocus={false}
                                ref='feedback'
                                placeHolderText='Your comments...'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                returnKey='done'
                                onChangeText={(feedback) => this.setState({ feedback })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={18}
                                multiline={true}
                                inputStyle={{height:  scaleHeight(160)}}
                                style={[styles.productBox, { height: scaleHeight(160), marginTop: 5, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }]}>
                            </FormTextInput>

                            <SubmitButton
                                onPress={() => this.sendFeedback()}
                                text="Submit"
                            />
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        elevation: 3,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
    },
})