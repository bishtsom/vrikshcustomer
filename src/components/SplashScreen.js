import React, { Component } from 'react'
import { ImageBackground, Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../utilities/Constants";
import { pushNotificationInit, pushNotificationRemove } from "../utilities/PushNotification";
import { NavigationActions, StackActions } from "react-navigation"

const { width } = Dimensions.get('window')
const resetActionHome = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});
const resetActionTutorialScreen = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'TutorialScreen' })
    ], key: null
});
export default class SplashScreen extends Component {
    constructor(props) {
        super()
        this.state = {
        }
    }

    componentWillMount(){
        pushNotificationInit();

    }

    componentDidMount() {

        setTimeout(() => {
            AsyncStorage.getItem("token").then((tokenValue) => {
                if (tokenValue == null || tokenValue == '') {
                    this.props.navigation.dispatch(resetActionTutorialScreen)
                } else {
                    this.props.navigation.dispatch(resetActionHome)
                }
            })
        }, 3000);
    }

    render() {
        return (
            <View
                style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
            >
                <Image
                    style={{ height: width - 20, width: width, justifyContent: 'center' }}
                    source={require('../assets/images/vriksh.png')}>

                </Image>
            </View>
            // <ImageBackground
            //     style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
            //     source={require('../assets/images/organic.jpg')}>
            // </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})