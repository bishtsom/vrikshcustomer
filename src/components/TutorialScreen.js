import { Dimensions, StyleSheet, View, Text, TouchableOpacity, ImageBackground, Modal, Image, SafeAreaView } from 'react-native';
import React, { Component } from 'react';
import SubmitButton from "../common/FormSubmitButton";
import Constants from "../utilities/Constants";
import { NavigationActions, StackActions } from "react-navigation"
import RestClient from '../utilities/RestClient';
import { Dropdown } from 'react-native-material-dropdown';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window')

const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Login' })
    ], key: null
});
const resetActionHome = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});
export default class TutorialScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            modalVisible: true,
            selectedCity: ''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("city").then((cityValue) => {
            if (cityValue == null || cityValue == '') {
                this.getCities()
            } else {
                this.setState({ modalVisible: false })
            }
        })
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
        }).catch(error => {
        });
    }

    closeModal() {
        if (this.state.selectedCity != null && this.state.selectedCity != '') {
            this.setState({ modalVisible: false });
            AsyncStorage.setItem("city", this.state.selectedCity)
        } else {
            alert('Please select your city')
        }
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    transparent
                    style={styles.modalContainer}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.closeModal()}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', width: width, height: height, alignSelf: 'center', justifyContent: 'center' }}>
                        <Dropdown
                            containerStyle={[{ width: width - 30, alignSelf: 'center' }]}
                            baseColor={Constants.Colors.White}
                            textColor={Constants.Colors.DarkBlue}
                            label='Select City'
                            fontSize={18}
                            data={this.state.cities}
                            onChangeText={(text) => { this.setState({ selectedCity: text }) }}
                        />
                        <SubmitButton
                            textStyle={{ fontWeight: '500' }}
                            backGroundStyle={{ backgroundColor: Constants.Colors.HeaderGreen, borderRadius: 50 }}
                            onPress={() => this.closeModal()}
                            style={{ marginHorizontal: 0, justifyContent: 'center', alignSelf: 'center' }}
                            text="Submit"
                        />
                    </View>
                </Modal>
                <ImageBackground source={require('../assets/images/tutorial.jpg')} style={{ flex: 1, position: 'relative', width: '100%', height: '100%' }}></ImageBackground>

                <View style={{ margin: 15, flexDirection: 'row', position: 'absolute', bottom: 150, justifyContent: 'center', alignSelf: 'center' }}>
                    <SubmitButton
                        textStyle={{ fontWeight: '500' }}
                        backGroundStyle={{ alignSelf: 'flex-start', backgroundColor: Constants.Colors.HeaderGreen, borderRadius: 50, width: 100 }}
                        onPress={() => this.props.navigation.dispatch(resetActionLogin)}
                        style={{ marginHorizontal: 0, justifyContent: 'flex-start', flex: .5, alignSelf: 'flex-start' }}
                        text="Next"
                    />
                    <SubmitButton
                        textStyle={{ color: 'black' }}
                        backGroundStyle={{ alignSelf: 'flex-end', backgroundColor: 'gray', color: 'black', borderRadius: 50, width: 100 }}
                        onPress={() => this.props.navigation.dispatch(resetActionHome)}
                        style={{ marginHorizontal: 0, justifyContent: 'flex-end', flex: .5, alignSelf: 'flex-end' }}
                        text="Skip"
                    />
                </View>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
    },
})