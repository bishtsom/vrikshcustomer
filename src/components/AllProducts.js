import React, { Component } from 'react'
import { FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Header from '../utilities/Header';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width } = Dimensions.get('window')

export default class AllProducts extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
            favDataList: [],
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getFavData();
    }

    getFavData() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/favourites", {}, tokenValue).then((result) => {
                console.log("favDataList new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ favDataList: result.fav_items })
                }
                this.getNewDetails();
            }).catch(error => {
                this.getNewDetails();
                alert(error)
            });
        })
    }

    getNewDetails() {
        let featuredList = [];
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("category/items", { city: 'dehradun', category_name: this.props.navigation.getParam('categoryName') }, this.state.tokenValue).then((result) => {
                if (result.success == "1") {
                    featuredList = result.Products.map(el => {
                        if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                            el.fav = true;
                        } else {
                            el.fav = false;
                        }
                        return el;
                    })
                    this.setState({ newOrders: featuredList })
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    addToFav(productId, key, fav) {
        if (this.state.tokenValue == null || this.state.tokenValue == '') {
            alert('Please Signin the application to add this item on favourites.')
        } else {
            let featuredList = []
            if (fav) {
                RestClient.delete("api/customer/favourites/remove", { product_id: productId }, this.state.tokenValue).then((result) => {
                    console.log("result", JSON.stringify(result))
                    if (result.success == "1") {
                        featuredList = this.state.newOrders.map(el => {
                            if (el.product_id === productId) {
                                el.fav = false;
                            }
                            return el;
                        })
                        this.setState({ newOrders: featuredList })
                        //alert(result.message)
                    } else {
                        alert(result.message)
                    }
                }).catch(error => {
                    alert(error)
                });
            } else {
                RestClient.post("api/customer/favourites/add", { product_id: productId }, this.state.tokenValue).then((result) => {
                    console.log("favourites add", JSON.stringify(result))
                    if (result.success == "1") {
                        featuredList = this.state.newOrders.map(el => {
                            if (el.product_id === productId) {
                                el.fav = true;
                            }
                            return el;
                        })
                        this.setState({ newOrders: featuredList })
                        //alert(result.message)
                    } else {
                        alert(result.message)
                    }
                }).catch(error => {
                    alert(error)
                });
            }
        }
    }

    openDraw() {
        this.props.navigation.openDrawer()
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={styles.containerSafe}>
                <Background style={styles.container}>
                    <Header
                        headerText="AllProducts"
                        onDrawerOpen={() => this.openDraw()}
                        goBackIcon={true}
                        navigation={this.props.navigation}
                    />
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView behavior={'position'}>
                            <View style={{ flexDirection: 'column', margin: 15 }}>
                                <FlatList
                                    data={this.state.newOrders}
                                    //maxToRenderPerBatch={5}
                                    renderItem={({ item, key }) => (
                                        <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { position: 'relative', margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 1.8 }]}>
                                            {item.offer ?
                                                <View style={{ position: 'absolute', top: -10, left: -10, justifyContent: 'center', width: width / 4, height: width / 12, backgroundColor: Constants.Colors.Orange, borderTopLeftRadius: 12, borderBottomRightRadius: 12 }}>
                                                    <Text style={{ alignSelf: 'center', justifyContent: 'center', fontSize: 16, color: Constants.Colors.White }}>{item.offer + ' %'}</Text>
                                                </View>
                                                :
                                                null
                                            }
                                            <Image
                                                style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center', marginTop: 30 }}
                                                // source={require('../assets/images/organic.jpg')}>
                                                source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                            </Image>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <View style={{ flex: .75, flexDirection: 'column' }}>
                                                    <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                        <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                    </View>
                                                </View>
                                                <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, key, item.fav)}>
                                                    {item.fav ?
                                                        <Icon name="heart" size={28} color="red" />
                                                        :
                                                        <Icon name="heart-o" size={28} color="red" />
                                                    }
                                                </TouchableOpacity>
                                            </View>
                                            {item.available ?
                                                null
                                                :
                                                <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                            }
                                        </TouchableOpacity>
                                    )}
                                    numColumns={2}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
        //backgroundColor: Constants.Colors.HeaderGreen
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})