import React, { Component } from 'react'
import { FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Header from '../utilities/Header';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';
import HeaderTemp from '../utilities/Header1';

const { width } = Dimensions.get('window')

export default class ProductList extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
            categorieList: [],
            showHeader: false
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
        this.props.navigation.addListener(
            'didFocus',
            payload => {
                console.log("Payload is called .....................")
                this.getCartDetails();
            }
        );
    }

    getCartDetails() {
        RestClient.get("api/customer/order/cart", {}, this.state.tokenValue).then((result) => {
            console.log("cartt new:", JSON.stringify(result));
            if (result.success == "1") {
                Constants.cartCount = result.cart_items.length;
                this.setState({ showHeader: true })
            }
        }).catch(error => {
            alert(error)
        });
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        let featuredList = []
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("category/items", { city: 'dehradun', category_name: this.props.navigation.getParam('productDetail') }, tokenValue).then((result) => {
                console.log("all products:", JSON.stringify(result));
                if (result.success == "1") {
                    featuredList = result.Products.map(el => {
                        el.quantity = 0;
                        return el;
                    })
                    this.setState({ newOrders: featuredList })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    openDraw() {
        this.props.navigation.openDrawer()
    }

    minus(productId, quantity) {
        if (quantity > 0) {
            let featuredList = []
            featuredList = this.state.newOrders.map(el => {
                if (el.product_id === productId) {
                    el.quantity = el.quantity - 1;
                }
                return el;
            })
            this.setState({ newOrders: featuredList })
        }
    }

    plus(productId) {
        let featuredList = []
        featuredList = this.state.newOrders.map(el => {
            if (el.product_id === productId) {
                el.quantity = el.quantity + 1;
            }
            return el;
        })
        this.setState({ newOrders: featuredList })
    }

    addToCart(productId, quantity, available) {
        if (this.state.tokenValue == null || this.state.tokenValue == '') {
            alert('Please Signin to the application for purchasing this item.')
        } else {
            if (available) {
                if (quantity > 0) {
                    RestClient.post("api/customer/order/cart/add", { product_id: productId, quantity: quantity }, this.state.tokenValue).then((result) => {
                        console.log("result", JSON.stringify(result))
                        if (result.success == "1") {
                            Constants.cartCount = Constants.cartCount + 1;
                            this.setState({ showHeader: true })
                            alert(result.message)
                        } else {
                            alert(result.message)
                        }
                    }).catch(error => {
                        alert(error)
                    });
                } else {
                    alert('Please add quantity of this item')
                }
            } else {
                alert('Please add quantity of this item')
            }
        }
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={styles.containerSafe}     >
                <Background style={styles.container}>
                    {this.state.showHeader ?
                        <Header
                            headerText="ProductList"
                            onDrawerOpen={() => this.openDraw()}
                            goBackIcon={true}
                            navigation={this.props.navigation}
                        />
                        :
                        <HeaderTemp
                            headerText="ProductList"
                            onDrawerOpen={() => this.openDraw()}
                            goBackIcon={true}
                            navigation={this.props.navigation}
                        />
                    }
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView behavior={'position'}>
                            <View style={{ flexDirection: 'column', margin: 15 }}>
                                <FlatList
                                    data={this.state.newOrders}
                                    //maxToRenderPerBatch={5}
                                    renderItem={({ item }) => (
                                        <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { margin: 10, justifyContent: 'space-around', flexDirection: 'row', height: width / 2 }]}>
                                            <View style={{ flexDirection: 'column', flex: .4, justifyContent: 'center', }}>
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, justifyContent: 'center', alignSelf: 'center' }}
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}
                                                >
                                                </Image>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </View>

                                            <View style={{ justifyContent: 'space-between', flexDirection: 'column', flex: .6, margin: 5 }}>
                                                <View>
                                                    <Text style={{ marginBottom: 5, fontSize: 16, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                    <Text style={{ fontSize: 12, color: Constants.Colors.BlurGrey }} numberOfLines={2} ellipsizeMode='tail'>{item.description}</Text>
                                                </View>
                                                <View>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                        <Text style={{ marginTop: 5, marginBottom: 5, marginLeft: 10, fontSize: 14, color: Constants.Colors.LightGray, }}>{item.unit}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                        <View style={{ flex: .8, flexDirection: 'row', }}>
                                                            <TouchableOpacity onPress={() => this.minus(item.product_id, item.quantity)}>
                                                                <Icon name="minus-square" size={28} color="red" />
                                                            </TouchableOpacity>
                                                            <Text style={{ marginLeft: 15, marginRight: 15, alignSelf: 'center', fontSize: 18 }}>{item.quantity}</Text>
                                                            <TouchableOpacity onPress={() => this.plus(item.product_id)}>
                                                                <Icon name="plus-square" size={28} color={Constants.Colors.HeaderGreen} />
                                                            </TouchableOpacity>
                                                        </View>
                                                        <TouchableOpacity style={{ flex: .2 }} onPress={() => this.addToCart(item.product_id, item.quantity, item.available)}>
                                                            <Icon name="shopping-cart" size={28} color={Constants.Colors.HeaderGreen} />
                                                        </TouchableOpacity>
                                                    </View>


                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                    //numColumns={2}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
        //backgroundColor: Constants.Colors.HeaderGreen
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})