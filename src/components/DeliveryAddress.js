import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../utilities/Regex';
import _ from "lodash";
import RestClient from '../utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../utilities/Header';
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')

export default class DeliveryAddress extends Component {
    constructor(props) {
        super()
        this.state = {
            city: '',
            locality: '',
            state: '',
            houseNo: '',
            landmark: '',
            pincode: '',
            tokenValue: '',
            cities: [],
            selectedCity: ''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getCities();
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
            this.getNewDetails();
        }).catch(error => {
            this.getNewDetails();
        });
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/profile", {}, tokenValue).then((result) => {
                console.log("customer details:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({
                        city: result.customer_detail.delivery_city,
                        selectedCity: result.customer_detail.delivery_city,
                        houseNo: result.customer_detail.delivery_houseNo,
                        landmark: result.customer_detail.delivery_landmark,
                        locality: result.customer_detail.delivery_locality,
                        pincode: result.customer_detail.delivery_pincode,
                        state: result.customer_detail.delivery_state,
                    })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    saveProfile(key) {
        Keyboard.dismiss()
        let { city, houseNo, selectedCity, locality, landmark, pincode, state } = this.state;
        let req = null
        if (key == '5') {
            if (_.isEmpty(houseNo && houseNo.trim())) {
                alert('Please enter your house number');
                return;
            }

            if (_.isEmpty(landmark && landmark.trim())) {
                alert('Please specfiy your address landmark');
                return;
            }

            if (_.isEmpty(locality && locality.trim())) {
                alert('Please specfiy your address locality');
                return;
            }

            if (_.isEmpty(selectedCity && selectedCity.trim())) {
                alert('Please specfiy your city');
                return;
            }

            if (_.isEmpty(state && state.trim())) {
                alert('Please specfiy your state');
                return;
            }

            if (_.isEmpty(pincode && pincode.trim())) {
                alert('Write your PinCode');
                return;
            }

            req = {
                address: "delivery",
                houseNo: houseNo,
                landmark: landmark,
                locality: locality,
                city: selectedCity.toLowerCase(),
                state: state,
                pincode: pincode,
            }
        }

        console.log("req", req)
        RestClient.put("api/customer/profile/update",
            req
            , this.state.tokenValue).then((result) => {
                console.log("result", result)
                if (result.success == "1") {
                    alert(result.message)
                    this.props.navigation.goBack();
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={styles.containerSafe}>
                <Background style={styles.container}>
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView style={{ flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>

                            <FormTextInput
                                autoFocus={false}
                                ref='houseNo'
                                placeHolderText='House No'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(houseNo) => this.setState({ houseNo })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.houseNo}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='landmark'
                                placeHolderText='Landmark'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(landmark) => this.setState({ landmark })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.landmark}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='locality'
                                placeHolderText='Locality'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(locality) => this.setState({ locality })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.locality}
                            />

                            {/* <FormTextInput
                                autoFocus={false}
                                ref='city'
                                placeHolderText='City'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(city) => this.setState({ city })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.city}
                            /> */}

                            <Dropdown
                                containerStyle={[{ width: width - 45, alignSelf: 'center' }]}
                                baseColor={Constants.Colors.Black}
                                textColor={Constants.Colors.Black}
                                selectedItemColor={Constants.Colors.Black}
                                label='Select City'
                                fontSize={16}
                                data={this.state.cities}
                                value={this.state.selectedCity}
                                onChangeText={(text) => { this.setState({ selectedCity: text }) }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='state'
                                placeHolderText='State'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(state) => this.setState({ state })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.state}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='pincode'
                                placeHolderText='PinCode'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(pincode) => this.setState({ pincode })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                                value={this.state.pincode}
                            />

                            <SubmitButton
                                onPress={() => this.saveProfile('5')}
                                text="Update"
                                style={{ alignSelf: 'flex-end', marginTop: 0 }}
                                backGroundStyle={{ width: width / 4, padding: 8 }}
                            />

                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})