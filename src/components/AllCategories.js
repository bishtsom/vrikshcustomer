import React, { Component } from 'react'
import { FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Header from '../utilities/Header';
import Connection from '../utilities/Connection';

const { width } = Dimensions.get('window')

export default class AllCategories extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            categorieList: []
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getAllCategories();
    }

    getAllCategories() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            let categoriesData = []
            RestClient.get("category", { city: 'dehradun', }, tokenValue).then((result) => {
                console.log("all categories:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ categorieList: result.All_Category })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    openDraw() {
        this.props.navigation.openDrawer()
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <SafeAreaView style={styles.containerSafe}     >
                <Background style={styles.container}>
                    <Header
                        headerText="AllCategories"
                        onDrawerOpen={() => this.openDraw()}
                        goBackIcon={true}
                        navigation={this.props.navigation}
                    />
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView behavior={'position'}>
                            <View style={{ flexDirection: 'column', margin: 15 }}>
                                <FlatList
                                    data={this.state.categorieList}
                                    renderItem={({ item }) => (
                                        <View style={{ flexDirection: 'column', width: width / 3.2, height: width / 2.3, }}>
                                            <TouchableOpacity onPress={() => navigate('ProductList', { productDetail: item.name })} style={[styles.productBox, { margin: 5, justifyContent: 'space-around', }]}>
                                                <Image
                                                    style={{ alignSelf: 'center', width: width / 4, height: width / 4, }}
                                                    // source={require('../assets/images/organic.jpg')}>
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                            </TouchableOpacity>
                                            <Text style={{ color: Constants.Colors.DarkGrey, marginTop: 5, fontSize: 14, marginLeft: 10 }} numberOfLines={2}>{item.name}</Text>
                                        </View>
                                    )}
                                    numColumns={3}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
        //backgroundColor: Constants.Colors.HeaderGreen
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})