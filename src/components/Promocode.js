import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';

const { width } = Dimensions.get('window')

export default class Promocode extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            aboutData: []
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("promos", {}, tokenValue).then((result) => {
                //alert("promos"+ JSON.stringify(result))
                if (result.success == "1") {
                    this.setState({ aboutData: result.promos })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        {this.state.aboutData && this.state.aboutData.length > 0 && this.state.aboutData.map((data, key) => {
                            return (
                                <View key={key} style={[styles.productBox,{margin: 5, flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.HeaderGreen }}>{data.code}</Text>
                                    <Text style={{ fontSize: 16, color: Constants.Colors.Black }}>{data.discount + " %"}</Text>
                                </View>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
         // alignSelf: 'center',
         alignItems: "center",
         height: 90,
         borderRadius: 10,
         padding: 8,
         backgroundColor: Constants.Colors.White,
         shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})