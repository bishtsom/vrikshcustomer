import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { scaleHeight, scaleWidth, normalizeFont } from "../utilities/Responsive";
import _ from "lodash";

const { width } = Dimensions.get('window')

export default class Complain extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            description: '',
            name: '',
            title: ''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    complain() {
        if (_.isEmpty(this.state.title && this.state.title.trim())) {
            alert('Please write your title');
            return;
        }

        if (_.isEmpty(this.state.description && this.state.description.trim())) {
            alert('Please write your complain description');
            return;
        }
        const { navigation } = this.props;
        const orderDetail = navigation.getParam('orderDetail');
        RestClient.post("api/customer/complain", { order_id: orderDetail.order_id, title: this.state.title, description: this.state.description, vendor_id: orderDetail.vendor_id }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
                this.props.navigation.goBack();
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        const { navigation } = this.props;
        const orderDetail = navigation.getParam('orderDetail');
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 10 }}>

                        <FormTextInput
                            autoFocus={false}
                            ref='title'
                            placeHolderText='Title'
                            placeHolderColor={Constants.Colors.DarkGrey}
                            secureText={false}
                            returnKey='done'
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(title) => this.setState({ title })}
                            textColor={Constants.Colors.DarkGrey}
                            fontSize={16}
                            style={{ marginVertical: 5 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='description'
                            placeHolderText='Your comments...'
                            placeHolderColor={Constants.Colors.DarkGrey}
                            returnKey='done'
                            onChangeText={(description) => this.setState({ description })}
                            textColor={Constants.Colors.DarkGrey}
                            fontSize={18}
                            multiline={true}
                            inputStyle={{ height: scaleHeight(160) }}
                            style={[ { height: scaleHeight(160), marginTop: 5, fontSize: 22, color: Constants.Colors.White, marginBottom: 15 }]}>
                        </FormTextInput>

                        <SubmitButton
                            onPress={() => this.complain()}
                            style={{ marginBottom: 10 }}
                            text="Submit"
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        // alignSelf: 'center',
        alignItems: "center",
        height: 90,
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})