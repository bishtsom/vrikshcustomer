import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../utilities/Regex';
import _ from "lodash";
import RestClient from '../utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from "react-navigation"
import { Dropdown } from 'react-native-material-dropdown';

const { width } = Dimensions.get('window')
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Login' })
    ], key: null
});

export default class SignUp extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            houseNo: '',
            landmark: '',
            locality: '',
            city: '',
            state: '',
            pincode: '',
            email: '',
            password: '',
            contact: '',
            secContact: '',
            cities: [],

        }
    }

    componentDidMount() {
        this.getCities();
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
        }).catch(error => {
        });
    }

    createDriver() {
        Keyboard.dismiss()
        let { name, houseNo, landmark, locality, contact, secContact, city, password, state, pincode, email } = this.state;

        if (_.isEmpty(name && name.trim())) {
            alert('Please enter your name');
            return;
        }

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        if (_.isEmpty(contact && contact.trim())) {
            alert('Please enter your contact');
            return;
        }

        if (_.isEmpty(houseNo && houseNo.trim())) {
            alert('Please enter your house number');
            return;
        }

        if (_.isEmpty(landmark && landmark.trim())) {
            alert('Please write your landmark location');
            return;
        }

        if (_.isEmpty(locality && locality.trim())) {
            alert('Please write your locality');
            return;
        }

        if (_.isEmpty(city && city.trim())) {
            alert('Enter your City name');
            return;
        }

        if (_.isEmpty(state && state.trim())) {
            alert('Please specfiy your address state');
            return;
        }

        if (_.isEmpty(pincode && pincode.trim())) {
            alert('Please write your pincode');
            return;
        }

        if (_.isEmpty(password && password.trim())) {
            alert('Write your password');
            return;
        }

        RestClient.post("api/customer/create", {
            name: name,
            email: email,
            contact_one: contact,
            contact_two: secContact,
            houseNo: houseNo,
            landmark: landmark,
            locality: locality,
            city: city.toLowerCase(),
            state: state,
            pincode: pincode,
            password: this.state.password,
        }).then((result) => {
            console.log("result", result)
            if (result.success == "1") {
                //AsyncStorage.setItem("city", city)
                alert(result.message)
                this.props.navigation.dispatch(resetActionLogin)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <View >
                    <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                        <KeyboardAvoidingView style={{ flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>

                            <Text style={{ color: Constants.Colors.HeaderGreen, textAlign: 'center', fontSize: 22, marginTop: 20 }}>CREATE PROFILE</Text>
                            
                            <FormTextInput
                                autoFocus={false}
                                ref='name'
                                placeHolderText='Name'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(name) => this.setState({ name })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='email'
                                placeHolderText='Email'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(email) => this.setState({ email })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='contact'
                                placeHolderText='Contact'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(contact) => this.setState({ contact })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='secContact'
                                placeHolderText='Secondary Contact'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(secContact) => this.setState({ secContact })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='houseNo'
                                placeHolderText='House No'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(houseNo) => this.setState({ houseNo })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='landmark'
                                placeHolderText='Landmark'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(landmark) => this.setState({ landmark })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='locality'
                                placeHolderText='Locality'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(locality) => this.setState({ locality })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            {/* <FormTextInput
                                autoFocus={false}
                                ref='city'
                                placeHolderText='City'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(city) => this.setState({ city })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            /> */}

                            <Dropdown
                                containerStyle={[{ width: width - 45, alignSelf: 'center' }]}
                                baseColor={Constants.Colors.Black}
                                textColor={Constants.Colors.Black}
                                selectedItemColor={Constants.Colors.Black}
                                label='Select City'
                                fontSize={16}
                                data={this.state.cities}
                                //value={this.state.selectedCity}
                                onChangeText={(text) => { this.setState({ city: text }) }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='state'
                                placeHolderText='State'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(state) => this.setState({ state })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='pincode'
                                placeHolderText='PinCode'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(pincode) => this.setState({ pincode })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='password'
                                placeHolderText='Password'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(password) => this.setState({ password })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={16}
                                style={{ marginVertical: 5 }}
                            />

                            <SubmitButton
                                onPress={() => this.createDriver()}
                                style={{ marginBottom: 10 }}
                                text="Signup"
                            />
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})