import React, { Component } from "react";
import {Share, Dimensions, Platform, StyleSheet, Text, View, Image, ImageBackground, SafeAreaView, TouchableOpacity, Button, ScrollView, ToastAndroid, Modal, TouchableHighlight } from "react-native";
import Constants from "../utilities/Constants";
import { NavigationActions, StackActions } from "react-navigation"
import AsyncStorage from '@react-native-community/async-storage';

const { height, width } = Dimensions.get('window');
const resetActionLogin = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Login' })
  ], key: null
});

export default class ProfileDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signedIn: false,
      name: 'Guest'
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("token").then((tokenValue) => {
      if (tokenValue == null || tokenValue == '') {
      } else {
        this.setState({ signedIn: true })
      }
    })
    AsyncStorage.getItem("name").then((name) => {
      if (name == null || name == '') {
        this.setState({ name: 'Guest' })
      } else {
        this.setState({ name: name })
      }
    })
  }

  logOut() {
    AsyncStorage.clear();
    this.props.navigation.dispatch(resetActionLogin)
  }

  shareApp() {
    try {
      const result = Share.share({
        title: 'App link',
        message: 'Please install this app and stay safe , AppLink :https://play.google.com/store/apps/details?id=com.vriksh.android&hl=en',
        url: 'https://play.google.com/store/apps/details?id=com.vriksh.android&hl=en'
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <SafeAreaView style={styles.containerSafe}>
        <ScrollView style={styles.container}>
          <View style={{ flex: .3, backgroundColor: Constants.Colors.HeaderGreen, flexDirection: 'column', justifyContent: 'center' }}>
            <View style={styles.navIcons}>
              <Text style={{ textAlign: 'center', alignSelf: 'center', justifyContent: 'center', fontSize: 22 }}>{this.state.name && this.state.name.charAt(0).toUpperCase()}</Text>
            </View>
            <Text style={[styles.textFields, { color: Constants.Colors.White, marginLeft: 50 }]}>Hi, {this.state.name}</Text>
          </View>
          <View style={{ flex: .7, margin: 20, justifyContent: 'space-between', flexDirection: 'column' }}>
            <View>
              <TouchableOpacity onPress={() => this.props.navigation.closeDrawer()}>
                <Text style={styles.textFields}>Home</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.state.signedIn ? navigate('MyProfile') : navigate('SignUp')}>
                <Text style={styles.textFields}>My profile</Text>
              </TouchableOpacity>
              {this.state.signedIn ?
                <View>
                  <TouchableOpacity onPress={() => navigate('CurrentOrders')}>
                    <Text style={styles.textFields}>Current Orders</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('OngoingOrders')}>
                    <Text style={styles.textFields}>Ongoing Orders</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('PreviousOrders')}>
                    <Text style={styles.textFields}>Previous Orders</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('Favourites')}>
                    <Text style={styles.textFields}>My Favourites</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('Promocode')}>
                    <Text style={styles.textFields}>Promocode</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.shareApp()}>
                    <Text style={styles.textFields}>Share</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('Feedback')}>
                    <Text style={styles.textFields}>Feedback</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => navigate('Faq')}>
                    <Text style={styles.textFields}>Faq</Text>
                  </TouchableOpacity>
                </View>
                :
                null}

              {/* <TouchableOpacity onPress={() => navigate('Favourites')}>
              <Text style={styles.textFields}>My address</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Profile')}>
              <Text style={styles.textFields}>Feedback</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Profile')}>
              <Text style={styles.textFields}>Contact</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Profile')}>
              <Text style={styles.textFields}>Share</Text>
            </TouchableOpacity> */}

            </View>

            <View style={{ justifyContent: 'flex-end' }}>
              <TouchableOpacity onPress={() => navigate('About')}>
                <Text style={[styles.textFields, { fontSize: 15 }]}>About</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('PrivacyPolicy')}>
                <Text style={[styles.textFields, { fontSize: 15 }]}>Privacy Policy</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.logOut()}>
                <Text style={styles.textFields}>{this.state.signedIn ? 'Log Out' : 'Log in'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerSafe: {
    flex: 1,
    backgroundColor: Constants.Colors.HeaderGreen
  },
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.White,
  },
  textFields: {
    color: Constants.Colors.DarkGrey,
    margin: 10,
    fontSize: 16
  },
  navIcons: {
    height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 28,
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 28,
    marginLeft: 20,
    marginTop: 20,
    justifyContent: 'center',
    borderColor: Constants.Colors.DarkBlue,
    borderWidth: 2,
    backgroundColor: Constants.Colors.White,
    borderRadius: (Constants.BaseStyle.DEVICE_WIDTH / 100 * 28) / 2
  },
});