import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Rating, AirbnbRating } from 'react-native-ratings';

const { width } = Dimensions.get('window')

export default class Favourites extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/favourites", {}, tokenValue).then((result) => {
                console.log("vendor new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ newOrders: result.fav_items })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    delete(productId) {
        RestClient.delete("api/customer/favourites/remove", { product_id: productId }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 10 }}>
                        {this.state.newOrders && this.state.newOrders.length > 0 && this.state.newOrders.map((data, key) => {
                            return (
                                <TouchableOpacity key={key} onPress={() => navigate('ProductDetails', { productDetail: data })} style={[styles.productBox, { margin: 2, flexDirection: 'row', flex: 1 }]}>
                                    <Image
                                        style={{ margin: 10, height: 50, width: 50 }}
                                        // source={require('../assets/images/organic.jpg')}>
                                        source={{ uri: `${Connection.getBaseUrl()}/${data.prof_img}` }}>
                                    </Image>
                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.DarkGrey }}>{data.product_name}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                            <Text style={{ fontSize: 14, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + data.price}</Text>
                                            <Text style={{ fontSize: 14, color: Constants.Colors.HeaderGreen }}>{'₹ ' + data.discounted_price}</Text>
                                        </View>
                                    </View>
                                    
                                </TouchableOpacity>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 90,
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})