import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, Alert, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import RadioForm from 'react-native-simple-radio-button';
import SubmitButton from "../common/FormSubmitButton";
import { NavigationActions, StackActions } from "react-navigation"
import FormTextInput from "../common/FormTextInput";
import DatePicker from 'react-native-datepicker'

const { width } = Dimensions.get('window')
var radio_props = [
    { label: 'Cash on delivery', value: 0 },
];
var delivery_props = [
    { label: 'Default delivery', value: 0 },
    { label: 'Select delivery date', value: 1 },
];
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});

export default class PlaceOrder extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            newOrders: [],
            deliveryCharges: 0,
            paymentTypeValue: 0,
            deliveryDateValue: 0,
            selectedDeliveryDate: '',
            promocode: '',
            promocodeDiscAmt: 0,
            deliveryAddress: '',
            date: new Date(),
        }
    }

    componentWillMount() {
        this.props.navigation.addListener(
            'didFocus',
            payload => {
                AsyncStorage.getItem("token").then((tokenValue) => {
                    console.log("tokkn are", tokenValue)
                    this.setState({ tokenValue: tokenValue })
                    this.getDeliveryDetails();
                })
            }
        );
    }

    getDeliveryDetails() {
        RestClient.get("api/customer/profile", {}, this.state.tokenValue).then((result) => {
            console.log("customer details:", JSON.stringify(result));
            if (result.success == "1") {
                this.setState({
                    deliveryAddress: result.customer_detail.delivery_houseNo + " " +
                        result.customer_detail.delivery_landmark + " " +
                        result.customer_detail.delivery_locality + " " +
                        result.customer_detail.delivery_city + " " +
                        result.customer_detail.delivery_state + " " +
                        result.customer_detail.delivery_pincode
                })
            } else {
                //alert(result.message)
            }
            this.getCities(result.customer_detail.delivery_city);
        }).catch(error => {
            this.getNewDetails();
            alert(error)
        });
    }

    getCities(city1) {
        let city = []
        RestClient.get("city/charge", { city: city1.toLowerCase() }).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                this.setState({ deliveryCharges: result.charge })
            }
            this.getNewDetails()
        }).catch(error => {
            this.getNewDetails()
        });
    }

    componentDidMount() {
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/order/cart", {}, tokenValue).then((result) => {
                console.log("cart new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ newOrders: result.cart_items })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    placeOrder() {
        if (this.state.deliveryDateValue == 1) {
            if (this.state.selectedDeliveryDate != '') {
                Alert.alert(
                    'Status',
                    'Are you sure, you want to confirm this order to be placed?',
                    [{
                        text: 'Confirm', onPress: () => {
                            this.confirmPlace();

                        }
                    },
                    { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    ],
                    { cancelable: false })
            } else {
                alert("Please select delivery date")
            }
        } else {
            Alert.alert(
                'Status',
                'Are you sure, you want to confirm this order to be placed?',
                [{
                    text: 'Confirm', onPress: () => {
                        this.confirmPlace();

                    }
                },
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                ],
                { cancelable: false })
        }
    }

    confirmPlace() {
       
        let totalAmount = 0
        for (let i = 0; i < this.state.newOrders.length; i++) {
            totalAmount = totalAmount + (this.state.newOrders[i].quantity * this.state.newOrders[i].discounted_price);
        }
        let list = [];
        list.push({
            delivery_method: this.state.deliveryDateValue == 0 ? "default" : this.state.selectedDeliveryDate,
            total_amount: this.state.promocodeDiscAmt == 0 ? (this.state.deliveryCharges + totalAmount) + "" : (this.state.deliveryCharges + totalAmount - ((this.state.deliveryCharges + totalAmount) * this.state.promocodeDiscAmt) / 100) + "",
        })
        for (let i = 0; i < this.state.newOrders.length; i++) {
            list.push({
                product_id: this.state.newOrders[i].product_id,
                quantity: this.state.newOrders[i].quantity + "",
                total_price: (this.state.newOrders[i].quantity * this.state.newOrders[i].discounted_price) + ""
            })
        }

        RestClient.post("api/customer/order/new", list, this.state.tokenValue).then((result) => {
            console.log("order create result", JSON.stringify(result))
            if (result.success == "1") {
                this.props.navigation.dispatch(resetActionLogin)
                alert('Your order has been successfully placed')
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    applyCode() {
        RestClient.get("api/customer/promo/check", { code: this.state.promocode }, this.state.tokenValue).then((result) => {
            //alert("dis: "+ JSON.stringify(result))
            if (result.success == "1") {
                this.setState({ promocodeDiscAmt: result.discount })
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    changeDeliveryAddress() {
        //this.setState({ deliveryAddress: this.state.deliveryAddress })
        this.props.navigation.navigate('DeliveryAddress')
    }

    render() {

        const { navigate } = this.props.navigation
        let totalItems = 0, totalAmount = 0
        for (let i = 0; i < this.state.newOrders.length; i++) {
            totalItems = totalItems + this.state.newOrders[i].quantity;
            totalAmount = totalAmount + (this.state.newOrders[i].quantity * this.state.newOrders[i].discounted_price);
        }
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView  >
                        <View style={[styles.productBox, { padding: 20, margin: 10, }]}>
                            <Text style={{ fontSize: 16, color: Constants.Colors.LightGray }}>{'Delivery Address'}</Text>
                            <FormTextInput
                                autoFocus={false}
                                ref='code'
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                editable={false}
                                //onChangeText={(deliveryAddress) => this.setState({ deliveryAddress })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={14}
                                style={{ marginVertical: 0 }}
                                value={this.state.deliveryAddress}
                            />
                            <SubmitButton
                                onPress={() => this.changeDeliveryAddress()}
                                text="Change"
                                style={{ alignSelf: 'flex-end', marginTop: 0, marginHorizontal: 10 }}
                                backGroundStyle={{ width: width / 4, padding: 8, marginTop: 5 }}
                            />
                        </View>

                        <View style={[styles.productBox, { padding: 20, margin: 10, flex: 1, }]}>
                            <Text style={{ fontSize: 16, color: Constants.Colors.LightGray, marginBottom: 15 }}>{'Order Details'}</Text>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Text style={{ flex: .4, fontSize: 14, color: Constants.Colors.Black }}>{'Product Name'}</Text>
                                <Text style={{ flex: .3, fontSize: 14, color: Constants.Colors.Black, textAlign: 'center' }}>{'Quantity'}</Text>
                                <Text style={{ flex: .3, fontSize: 14, color: Constants.Colors.Black, textAlign: 'right' }}>{'Price'}</Text>
                            </View>
                            {this.state.newOrders && this.state.newOrders.length > 0 && this.state.newOrders.map((data, key) => {
                                return (
                                    <View key={key} style={{ flexDirection: 'row', flex: 1, marginTop: 5 }}>
                                        <Text style={{ flex: .4, fontSize: 14, color: Constants.Colors.HeaderGreen }}>{data.product_name}</Text>
                                        <Text style={{ flex: .3, fontSize: 14, color: Constants.Colors.HeaderGreen, textAlign: 'center' }}>{data.quantity}</Text>
                                        <Text style={{ flex: .3, fontSize: 14, color: Constants.Colors.HeaderGreen, textAlign: 'right' }}>{'₹' + (data.quantity * data.discounted_price)}</Text>
                                    </View>
                                )
                            })}
                            <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                                <Text style={{ flex: .5, fontSize: 14, color: Constants.Colors.LightGray, textAlign: 'left' }}>{'Total'}</Text>
                                <Text style={{ flex: .5, fontSize: 14, color: Constants.Colors.LightGray, textAlign: 'right' }}>{'₹' + totalAmount}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                                <Text style={{ flex: .5, fontSize: 14, color: Constants.Colors.LightGray, textAlign: 'left' }}>{'Delivery Charge'}</Text>
                                <Text style={{ flex: .5, fontSize: 14, color: Constants.Colors.LightGray, textAlign: 'right' }}>{'₹' + this.state.deliveryCharges}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                                <Text style={{ fontWeight: '700', flex: .5, fontSize: 18, color: Constants.Colors.HeaderGreen, textAlign: 'left' }}>{'Total'}</Text>
                                <Text style={{ fontWeight: '700', flex: .5, fontSize: 18, color: Constants.Colors.HeaderGreen, textAlign: 'right' }}>{'₹' + (this.state.deliveryCharges + totalAmount)}</Text>
                            </View>
                        </View>

                        <View style={[styles.productBox, { padding: 20, margin: 10, }]}>
                            <Text style={{ fontSize: 16, color: Constants.Colors.LightGray }}>{'Have promocode?'}</Text>
                            <FormTextInput
                                autoFocus={false}
                                ref='code'
                                placeHolderText='Promocode'
                                placeHolderColor={Constants.Colors.DarkGrey}
                                secureText={false}
                                //keyboard='email-address'
                                returnKey='done'
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(promocode) => this.setState({ promocode })}
                                textColor={Constants.Colors.DarkGrey}
                                fontSize={14}
                                style={{ marginVertical: 0 }}
                            />
                            <SubmitButton
                                onPress={() => this.applyCode()}
                                text="Apply"
                                style={{ alignSelf: 'flex-end', marginTop: 0, marginHorizontal: 10 }}
                                backGroundStyle={{ width: width / 4, padding: 8, marginTop: 5 }}
                            />
                        </View>

                        <View style={[styles.productBox, { padding: 20, margin: 10, }]}>
                            <Text style={{ marginBottom: 10, fontSize: 16, color: Constants.Colors.LightGray }}>{'Payment Method'}</Text>
                            <RadioForm
                                radio_props={radio_props}
                                initial={0}
                                buttonColor={Constants.Colors.HeaderGreen}
                                buttonInnerColor={Constants.Colors.HeaderGreen}
                                animation={true}
                                buttonSize={14}
                                buttonOuterSize={24}
                                buttonStyle={{ margin: 5 }}
                                labelStyle={{ margin: 5, fontSize: 14, color: Constants.Colors.DarkGrey }}
                                onPress={(value) => { this.setState({ paymentTypeValue: value }) }}
                            />
                        </View>

                        <View style={[styles.productBox, { padding: 20, margin: 10, }]}>
                            <Text style={{ marginBottom: 10, fontSize: 16, color: Constants.Colors.LightGray }}>{'Delivery Date'}</Text>

                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <RadioForm
                                    radio_props={delivery_props}
                                    initial={0}
                                    buttonColor={Constants.Colors.HeaderGreen}
                                    buttonInnerColor={Constants.Colors.HeaderGreen}
                                    animation={true}
                                    buttonSize={14}
                                    buttonOuterSize={24}
                                    buttonStyle={{ margin: 5 }}
                                    labelStyle={{ margin: 5, fontSize: 14, color: Constants.Colors.DarkGrey }}
                                    onPress={(value) => { this.setState({ deliveryDateValue: value }) }}
                                />

                                {this.state.deliveryDateValue == 0 ?
                                    null
                                    :
                                    <DatePicker
                                        date={this.state.selectedDeliveryDate}
                                        mode='date'
                                        style={{ alignSelf: 'flex-end', justifyContent: 'flex-end' }}
                                        minDate={this.state.date}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        placeholder="Select date"
                                        format="DD-MM-YYYY"
                                        customStyles={{
                                            btnTextText: {
                                                color: Constants.Colors.LightBlue,
                                                fontSize: 40,
                                            },
                                            dateInput: {
                                                alignItems: 'flex-start',
                                                color: Constants.Colors.LightBlue,
                                                borderWidth: 0
                                            },
                                            dateText: {
                                                color: Constants.Colors.LightBlue,
                                                justifyContent: 'flex-start',
                                                fontSize: 18,
                                            },
                                            placeholderText: {
                                                fontSize: 18,
                                                color: Constants.Colors.LightBlue,
                                                width: width
                                            }
                                        }}
                                        onDateChange={date => this.setState({ selectedDeliveryDate: date })}
                                    />
                                }
                            </View>
                        </View>

                        <SubmitButton
                            onPress={() => this.placeOrder()}
                            style={{ marginBottom: 10 }}
                            //onPress={() => this.props.navigation.navigate("Home")}
                            text={this.state.promocodeDiscAmt == 0 ? "Place Order" + ' (₹' + (this.state.deliveryCharges + totalAmount) + ")" : "Place Order" + ' (₹' + (this.state.deliveryCharges + totalAmount - ((this.state.deliveryCharges + totalAmount) * this.state.promocodeDiscAmt) / 100) + ")"}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        elevation: 3,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
    },
})