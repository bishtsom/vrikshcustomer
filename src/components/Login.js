import React, { Component } from 'react'
import { Modal, Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../utilities/Regex';
import _ from "lodash";
import RestClient from '../utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from "react-navigation"
import Icon from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get('window')
const resetActionHome = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});
export default class Login extends Component {
    constructor(props) {
        super()
        this.state = {
            email: '',
            password: '',
            forgotVisible: false,
            forgotEmail: ''
            //email: 'ss@ss.com',
            //password: '0000',
        }
    }

    componentDidMount() {
    }

    loginUser() {
        Keyboard.dismiss()
        let { email, password } = this.state;

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        if (_.isEmpty(password)) {
            alert('Please enter your password');
            return;
        }

        // AsyncStorage.getItem("pushToken").then((pushToken) => {
        console.log("push token are", Constants.registrationId)
        RestClient.post("login/customer", { email: this.state.email, password: this.state.password, registration_id: Constants.registrationId }).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                AsyncStorage.setItem("token", result.token)
                AsyncStorage.setItem("name", result.name)
                console.log("token", JSON.stringify(result.token))
                this.props.navigation.dispatch(resetActionHome)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
        // })
    }

    forgotPassword() {
        Keyboard.dismiss()
        let { forgotEmail } = this.state;

        if (_.isEmpty(forgotEmail && forgotEmail.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(forgotEmail && forgotEmail.trim())) {
            alert('Enter a valid email');
            return;
        }

        this.setState({ forgotVisible: false })
        setTimeout(() => {
            alert('Your password has been changed successfull. Please check your email account.')
        }, 100);
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        <Image source={Constants.Images.vriksh} style={styles.logo} resizeMode={'contain'} />
                        {/* <Text style={{ color: Constants.Colors.HeaderGreen, textAlign: 'center', fontSize: 22 }}>Customer App</Text> */}
                        <FormTextInput
                            autoFocus={false}
                            ref='email'
                            placeHolderText='Email'
                            placeHolderColor={Constants.Colors.DarkGrey}
                            secureText={false}
                            keyboard='email-address'
                            returnKey='done'
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(email) => this.setState({ email })}
                            textColor={Constants.Colors.DarkGrey}
                            fontSize={18}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='password'
                            placeHolderText='Password'
                            placeHolderColor={Constants.Colors.DarkGrey}
                            returnKey='done'
                            secureText={true}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(password) => this.setState({ password })}
                            textColor={Constants.Colors.DarkGrey}
                            fontSize={18}
                        />

                        <TouchableOpacity onPress={() => this.setState({ forgotVisible: true })}>
                            <Text style={{ color: Constants.Colors.HeaderGreen, alignSelf: 'flex-end', marginRight: 20 }}>Forgot Password?</Text>
                        </TouchableOpacity>

                        <SubmitButton
                            onPress={() => this.loginUser()}
                            text="Login"
                        />

                        <SubmitButton
                            onPress={() => navigate("SignUp")}
                            text="Signup"
                        />

                        <Modal
                            animationType={"fade"}
                            transparent={true}
                            visible={this.state.forgotVisible}
                            style={{ alignSelf: 'center', justifyContent: 'center', flex: 1 }}
                            onRequestClose={() => { this.setState({ forgotVisible: false }) }}>
                            {/*All views of Modal*/}
                            <View style={styles.modal}>
                                    <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => { this.setState({ forgotVisible: false }) }} >
                                        <Icon name="close" size={30} color={Constants.Colors.White} />
                                    </TouchableOpacity>

                                    <Text style={{color:Constants.Colors.White, alignSelf:'center', fontSize: 18}}>Forgot Password?</Text>
                                    <FormTextInput
                                        autoFocus={false}
                                        ref='email'
                                        placeHolderText='Email'
                                        placeHolderColor={Constants.Colors.White}
                                        secureText={false}
                                        keyboard='email-address'
                                        returnKey='done'
                                        isPassword={false}
                                        showPassword={false}
                                        onChangeText={(forgotEmail) => this.setState({ forgotEmail })}
                                        textColor={Constants.Colors.White}
                                        fontSize={18}
                                    />

                                    <SubmitButton
                                        onPress={() => this.forgotPassword()}
                                        text="Submit"
                                        style={{ marginBottom: 30 }}
                                        backGroundStyle={{ backgroundColor: Constants.Colors.White, width: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 40 }}
                                        textStyle={{ color: Constants.Colors.HeaderGreen }}
                                    />
                            </View>
                        </Modal>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 50,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 50,
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    register: {
        fontSize: 16,
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    modal: {
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: Constants.Colors.HeaderGreen,
        width: '80%',
        borderRadius: 10,
        top:height/4,
    },
})