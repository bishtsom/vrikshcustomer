import React, { Component } from 'react'
import { TextInput, TouchableHighlight, FlatList, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import Header from '../utilities/Header';
import HeaderTemp from '../utilities/Header1';
import Connection from '../utilities/Connection';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImageSlider from 'react-native-image-slider';
import { Dropdown } from 'react-native-material-dropdown';

// import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'react-native-best-viewpager';

const { width } = Dimensions.get('window')

export default class Home extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            offerOrders: [],
            newOrders: [],
            categorieList: [],
            uttarakhandOrders: [],
            featuredOrders: [],
            favDataList: [],
            seasonalOrders: [],
            organicOrders: [],
            showProductsList: false,
            showHeader: false,
            bannerImages: [],
            cities: [],
            selectedCity: ''

        }
        this.arrayholder = [];
    }

    componentWillMount() {
        this.props.navigation.addListener(
            'didFocus',
            payload => {
                AsyncStorage.getItem("token").then((tokenValue) => {
                    console.log("tokkn are", tokenValue)
                    this.setState({ tokenValue: tokenValue })
                    this.getCartDetails();
                })
            }
        );
        AsyncStorage.getItem("city").then((cityValue) => {
            this.setState({ selectedCity: cityValue })
        })
        this.getBannerImages()
    }

    getBannerImages() {
        let img = []
        RestClient.get("banner/images", {}).then((result) => {
            console.log("banner new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.images.length; i++) {
                    img.push(Connection.getBaseUrl() + result.images[i])
                }
                this.setState({ bannerImages: img })
            }
            this.getFavData();
        }).catch(error => {
            this.getFavData();
            alert(error)
        });
    }

    getCartDetails() {
        RestClient.get("api/customer/order/cart", {}, this.state.tokenValue).then((result) => {
            console.log("cartt new:", JSON.stringify(result));
            if (result.success == "1") {
                Constants.cartCount = result.cart_items.length;
                this.setState({ showHeader: true })
            } else {
                Constants.cartCount = 0;
                this.setState({ showHeader: true })
            }
        }).catch(error => {
            alert(error)
        });
    }

    getFavData() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/favourites", {}, tokenValue).then((result) => {
                console.log("favDataList new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ favDataList: result.fav_items })
                }
                this.getCities();
            }).catch(error => {
                this.getCities();
                alert(error)
            });
        })
    }

    getCities() {
        let city = []
        RestClient.get("city", {}).then((result) => {
            console.log("city new:", JSON.stringify(result));
            if (result.success == "1") {
                for (let i = 0; i < result.cities.length; i++) {
                    city.push({ value: result.cities[i].charAt(0).toUpperCase() + result.cities[i].slice(1) })
                }
            }
            this.setState({ cities: city })
            this.getAllCategories();
        }).catch(error => {
            this.getAllCategories();
        });
    }

    getAllCategories() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            let categoriesData = []
            RestClient.get("category", { city: this.state.selectedCity.toLowerCase(), }, tokenValue).then((result) => {
                console.log("all categories:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ categorieList: result.All_Category })
                } else {
                    //alert(result.message)
                }
                this.getOffers();
            }).catch(error => {
                this.getOffers();
                alert(error)
            });
        })
    }

    getAllProducts() {
        let featuredList = []
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            // let categoriesData = []
            RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'all' }, tokenValue).then((result) => {
                console.log("all products:", JSON.stringify(result));
                if (result.success == "1") {
                    featuredList = result.Products.map(el => {
                        if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                            el.fav = true;
                        } else {
                            el.fav = false;
                        }
                        return el;
                    })
                    this.setState({ newOrders: featuredList })
                    this.arrayholder = result.Products;

                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    getOffers() {
        let featuredList = []
        RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'offer' }, this.state.tokenValue).then((result) => {
            console.log("all products:", JSON.stringify(result));
            if (result.success == "1") {
                featuredList = result.Products.map(el => {
                    if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                        el.fav = true;
                    } else {
                        el.fav = false;
                    }
                    return el;
                })
                this.setState({ offerOrders: featuredList })
            } else {
                //alert(result.message)
            }
            this.getUttarakhand()
        }).catch(error => {
            this.getUttarakhand()
            alert(error)
        });
    }

    getUttarakhand() {
        let featuredList = []
        RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'uttrakhand' }, this.state.tokenValue).then((result) => {
            console.log("all products:", JSON.stringify(result));
            if (result.success == "1") {
                featuredList = result.Products.map(el => {
                    if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                        el.fav = true;
                    } else {
                        el.fav = false;
                    }
                    return el;
                })
                this.setState({ uttarakhandOrders: featuredList })
            } else {
                //alert(result.message)
            }
            this.getFeatured()
        }).catch(error => {
            this.getFeatured()
            alert(error)
        });
    }

    getFeatured() {
        let featuredList = []
        RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'featured' }, this.state.tokenValue).then((result) => {
            if (result.success == "1") {
                featuredList = result.Products.map(el => {
                    if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                        el.fav = true;
                    } else {
                        el.fav = false;
                    }
                    return el;
                })
                this.setState({ featuredOrders: featuredList })
            } else {
                //alert(result.message)
            }
            this.getOrganic()
        }).catch(error => {
            this.getOrganic()
            alert(error)
        });
    }

    getOrganic() {
        let featuredList = []
        RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'organic' }, this.state.tokenValue).then((result) => {
            console.log("all products:", JSON.stringify(result));
            if (result.success == "1") {
                featuredList = result.Products.map(el => {
                    if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                        el.fav = true;
                    } else {
                        el.fav = false;
                    }
                    return el;
                })
                this.setState({ organicOrders: featuredList })
            } else {
                //alert(result.message)
            }
            this.getSeasonal()
        }).catch(error => {
            this.getSeasonal()
            alert(error)
        });
    }

    getSeasonal() {
        let featuredList = []
        RestClient.get("category/items", { city: this.state.selectedCity.toLowerCase(), category_name: 'seasonal' }, this.state.tokenValue).then((result) => {
            console.log("all products:", JSON.stringify(result));
            if (result.success == "1") {
                featuredList = result.Products.map(el => {
                    if (this.state.favDataList.find(el2 => el.product_id === el2.product_id)) {
                        el.fav = true;
                    } else {
                        el.fav = false;
                    }
                    return el;
                })
                this.setState({ seasonalOrders: featuredList })
            } else {
                //alert(result.message)
            }
            this.getAllProducts();
        }).catch(error => {
            this.getAllProducts();
            alert(error)
        });
    }

    openDraw() {
        this.props.navigation.openDrawer()
    }

    addToFav(productId, listType, fav) {
        if (this.state.tokenValue == null || this.state.tokenValue == '') {
            alert('Please Signin the application to add this item on favourites.')
        } else {
            let featuredList = [], mainList = []
            if (listType == '1') {
                mainList = this.state.offerOrders;
            } else if (listType == '2') {
                mainList = this.state.uttarakhandOrders;
            } else if (listType == '3') {
                mainList = this.state.featuredOrders;
            } else if (listType == '4') {
                mainList = this.state.organicOrders;
            } else if (listType == '5') {
                mainList = this.state.seasonalOrders;
            } else if (listType == '6') {
                mainList = this.state.newOrders;
            }
            if (fav) {
                RestClient.delete("api/customer/favourites/remove", { product_id: productId }, this.state.tokenValue).then((result) => {
                    console.log("result", JSON.stringify(result))
                    if (result.success == "1") {
                        featuredList = mainList.map(el => {
                            if (el.product_id === productId) {
                                el.fav = false;
                            }
                            return el;
                        })
                        if (listType == '1') {
                            this.setState({ offerOrders: featuredList })
                        } else if (listType == '2') {
                            this.setState({ uttarakhandOrders: featuredList })
                        } else if (listType == '3') {
                            this.setState({ featuredOrders: featuredList })
                        } else if (listType == '4') {
                            this.setState({ organicOrders: featuredList })
                        } else if (listType == '5') {
                            this.setState({ seasonalOrders: featuredList })
                        } else if (listType == '6') {
                            this.setState({ newOrders: featuredList })
                        }
                    } else {
                        alert(result.message)
                    }
                }).catch(error => {
                    alert(error)
                });
            } else {
                RestClient.post("api/customer/favourites/add", { product_id: productId }, this.state.tokenValue).then((result) => {
                    console.log("favourites add", JSON.stringify(result))
                    if (result.success == "1") {
                        featuredList = mainList.map(el => {
                            if (el.product_id === productId) {
                                el.fav = true;
                            }
                            return el;
                        })
                        if (listType == '1') {
                            this.setState({ offerOrders: featuredList })
                        } else if (listType == '2') {
                            this.setState({ uttarakhandOrders: featuredList })
                        } else if (listType == '3') {
                            this.setState({ featuredOrders: featuredList })
                        } else if (listType == '4') {
                            this.setState({ organicOrders: featuredList })
                        } else if (listType == '5') {
                            this.setState({ seasonalOrders: featuredList })
                        } else if (listType == '6') {
                            this.setState({ newOrders: featuredList })
                        }
                    } else {
                        alert(result.message)
                    }
                }).catch(error => {
                    alert(error)
                });
            }
        }
    }

    searchFilterFunction(text) {
        console.log("text", text)
        //this.getFavData()

        if (text.length > 0) {
            this.setState({ showProductsList: true })
        } else {
            this.setState({ showProductsList: false })
        }
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.product_name ? item.product_name.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            newOrders: newData,
            //searchText: text,
        });
    }

    renderBannerView() {
        // const images = [
        //     'https://placeimg.com/640/640/nature',
        //     'https://source.unsplash.com/1024x768/?tree',
        //     'https://source.unsplash.com/1024x768/?water',
        //     'https://placeimg.com/640/640/beer',
        // ];
        return (
            <View style={styles.customSlide}>
                <ImageSlider
                    loopBothSides
                    autoPlayWithInterval={5000}
                    images={this.state.bannerImages}
                    customSlide={({ index, item, style, width }) => (
                        // It's important to put style here because it's got offset inside
                        <View key={index} style={[style, styles.customSlide]}>
                            <Image source={{ uri: item }} style={styles.customImage} />
                        </View>
                    )}
                    customButtons={(position, move) => (
                        <View style={styles.buttons}>
                            {this.state.bannerImages.map((image, index) => {
                                return (
                                    <TouchableHighlight
                                        key={index}
                                        underlayColor={Constants.Colors.Gray}
                                        //onPress={() => move(index)}
                                        style={styles.button}
                                    >
                                        <View style={position === index && styles.buttonSelected}>
                                        </View>
                                    </TouchableHighlight>
                                );
                            })}
                        </View>
                    )}
                />
            </View>
        )
    }

    changeCity(city){
        this.setState({selectedCity: city})
        AsyncStorage.setItem("city", city)
        this.getAllCategories();
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <SafeAreaView style={styles.containerSafe}     >
                <Background style={styles.container}>
                    <View style={{ position: 'relative', marginBottom: 50, }}>
                        {this.state.showHeader ?
                            <Header
                                headerText="Home"
                                onDrawerOpen={() =>
                                    this.openDraw()
                                }
                                goBackIcon={false}
                                navigation={this.props.navigation}
                            /> :
                            <HeaderTemp
                                headerText="Home"
                                onDrawerOpen={() =>
                                    this.openDraw()
                                }
                                goBackIcon={false}
                                navigation={this.props.navigation}
                            />
                        }

                        <View style={styles.textInputStyle}>
                            <Icon name="search" size={20} color={Constants.Colors.LightGrey} style={{ flex: .05, alignSelf: 'center', justifyContent: 'center', marginRight: 5, marginLeft: 15 }} />
                            <TextInput
                                style={{ fontSize: 16, marginLeft: 5, paddingLeft: 5, flex: .95 }}
                                onChangeText={text => this.searchFilterFunction(text)}
                                value={this.state.text}
                                underlineColorAndroid="transparent"
                                placeholder="Search.."
                                placeholderTextColor={Constants.Colors.DarkGrey}
                            />
                        </View>
                    </View>

                    <Dropdown
                        containerStyle={[{ width: width - 40, alignSelf: 'center' }]}
                        baseColor={Constants.Colors.DarkGrey}
                        textColor={Constants.Colors.DarkBlue}
                        label='Select City'
                        fontSize={18}
                        data={this.state.cities}
                        value={this.state.selectedCity}
                        onChangeText={(text) =>  this.changeCity(text) }
                    />

                    <ScrollView >
                        {!this.state.showProductsList ?
                            <View style={{ backgroundColor: Constants.Colors.White }}>

                                {this.renderBannerView()}

                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Easy Fast {"\n"}Home Services</Text>
                                        <TouchableOpacity onPress={() => navigate('AllCategories')}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View >
                                    <FlatList
                                        data={this.state.categorieList.slice(0, 6)}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item }) => (
                                            <View style={{ flexDirection: 'column', width: width / 3.2, height: width / 2.3 }}>
                                                <TouchableOpacity onPress={() => navigate('ProductList', { productDetail: item.name })} style={[styles.productBox, { margin: 5, justifyContent: 'space-around', }]}>
                                                    <Image
                                                        style={{ alignSelf: 'center', width: width / 4, height: width / 4, }}
                                                        // source={require('../assets/images/organic.jpg')}>
                                                        source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                    </Image>
                                                </TouchableOpacity>
                                                <Text style={{ color: Constants.Colors.DarkGrey, marginTop: 5, fontSize: 14, marginLeft: 10 }} numberOfLines={2}>{item.name}</Text>
                                            </View>
                                        )}
                                        numColumns={3}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Ongoing Offers</Text>
                                        <TouchableOpacity onPress={() => navigate('AllProducts', { categoryName: 'offer' })}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View>
                                    <FlatList
                                        data={this.state.offerOrders.slice(0, 5)}
                                        //maxToRenderPerBatch={5}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item, key }) => (
                                            <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { position: 'relative', margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 1.9 }]}>
                                                {item.offer ?
                                                    <View style={{ position: 'absolute', top: -10, left: -10, justifyContent: 'center', width: width / 4, height: width / 12, backgroundColor: Constants.Colors.Orange, borderTopLeftRadius: 12, borderBottomRightRadius: 12 }}>
                                                        <Text style={{ alignSelf: 'center', justifyContent: 'center', fontSize: 16, color: Constants.Colors.White }}>{item.offer + ' %'}</Text>
                                                    </View>
                                                    :
                                                    null
                                                }
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center', marginTop: 30 }}
                                                    // source={require('../assets/images/organic.jpg')}>
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: .75, flexDirection: 'column' }}>
                                                        <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, '1', item.fav)}>
                                                        {item.fav ?
                                                            <Icon name="heart" size={28} color="red" />
                                                            :
                                                            <Icon name="heart-o" size={28} color="red" />
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </TouchableOpacity>
                                        )}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                {/* {this.renderBannerView()} */}

                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Uttarakhand Special</Text>
                                        <TouchableOpacity onPress={() => navigate('AllProducts', { categoryName: 'uttrakhand' })}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View>
                                    <FlatList
                                        data={this.state.uttarakhandOrders.slice(0, 5)}
                                        //maxToRenderPerBatch={5}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item, key }) => (
                                            <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 2 }]}>
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center' }}
                                                    // source={require('../assets/images/organic.jpg')}>
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: .75, flexDirection: 'column' }}>
                                                        <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, '2', item.fav)}>
                                                        {item.fav ?
                                                            <Icon name="heart" size={28} color="red" />
                                                            :
                                                            <Icon name="heart-o" size={28} color="red" />
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </TouchableOpacity>
                                        )}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                {/* {this.renderBannerView()} */}

                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Featured Products</Text>
                                        <TouchableOpacity onPress={() => navigate('AllProducts', { categoryName: 'featured' })}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View>
                                    <FlatList
                                        data={this.state.featuredOrders.slice(0, 5)}
                                        //maxToRenderPerBatch={5}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item, key }) => (
                                            <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 2 }]}>
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center' }}
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: .75, flexDirection: 'column' }}>
                                                        <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, '3', item.fav)}>
                                                        {item.fav ?
                                                            <Icon name="heart" size={28} color="red" />
                                                            :
                                                            <Icon name="heart-o" size={28} color="red" />
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </TouchableOpacity>
                                        )}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                {/* {this.renderBannerView()} */}
                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Truly Organic</Text>
                                        <TouchableOpacity onPress={() => navigate('AllProducts', { categoryName: 'organic' })}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View>
                                    <FlatList
                                        data={this.state.organicOrders.slice(0, 5)}
                                        //maxToRenderPerBatch={5}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item, key }) => (
                                            <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 2 }]}>
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center' }}
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: .75, flexDirection: 'column' }}>
                                                        <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, '4', item.fav)}>
                                                        {item.fav ?
                                                            <Icon name="heart" size={28} color="red" />
                                                            :
                                                            <Icon name="heart-o" size={28} color="red" />
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </TouchableOpacity>
                                        )}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                {/* {this.renderBannerView()} */}
                                <View style={{ flexDirection: 'column', margin: 15, }}>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>Seasonal Products</Text>
                                        <TouchableOpacity onPress={() => navigate('AllProducts', { categoryName: 'seasonal' })}>
                                            <Text style={{ fontSize: 16, color: Constants.Colors.DarkGrey }}>View all ></Text>
                                        </TouchableOpacity>
                                    </View>
                                    <FlatList
                                        data={this.state.seasonalOrders.slice(0, 5)}
                                        //maxToRenderPerBatch={5}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({ item, key }) => (
                                            <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 2 }]}>
                                                <Image
                                                    style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center' }}
                                                    source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                                </Image>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View style={{ flex: .75, flexDirection: 'column' }}>
                                                        <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                            <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                        </View>
                                                    </View>
                                                    <TouchableOpacity style={{ flex: .25 }} onPress={() => this.addToFav(item.product_id, '5', item.fav)}>
                                                        {item.fav ?
                                                            <Icon name="heart" size={28} color="red" />
                                                            :
                                                            <Icon name="heart-o" size={28} color="red" />
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                                {item.available ?
                                                    null
                                                    :
                                                    <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                                }
                                            </TouchableOpacity>
                                        )}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>

                                {/* <View > */}
                                {/* <Text style={{ fontSize: 18, color: Constants.Colors.Black, margin: 5, marginLeft: 15, }}>Three easy steps</Text> */}
                                <ImageBackground source={require('../assets/images/bottom_ban.jpg')} style={{ height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 20, width: '100%', resizeMode: 'cover' }}></ImageBackground>
                                {/* </View> */}
                                {/* </View> */}
                            </View>
                            :
                            <FlatList
                                data={this.state.newOrders}
                                renderItem={({ item, key }) => (
                                    <TouchableOpacity onPress={() => navigate('ProductDetails', { productDetail: item })} style={[styles.productBox, { position: 'relative', margin: 12, justifyContent: 'space-around', width: width / 2.5, height: width / 2 }]}>
                                        {item.offer ?
                                            <View style={{ position: 'absolute', top: -10, left: -10, justifyContent: 'center', width: width / 4, height: width / 12, backgroundColor: Constants.Colors.Orange, borderTopLeftRadius: 12, borderBottomRightRadius: 12 }}>
                                                <Text style={{ alignSelf: 'center', justifyContent: 'center', fontSize: 16, color: Constants.Colors.White }}>{item.offer + ' %'}</Text>
                                            </View>
                                            :
                                            null
                                        }
                                        <Image
                                            style={{ width: width / 4, height: width / 4, alignSelf: 'center', justifyContent: 'center', marginTop: 30 }}
                                            source={{ uri: `${Connection.getBaseUrl()}/${item.prof_img}` }}>
                                        </Image>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <View style={{ flex: .75, flexDirection: 'column' }}>
                                                <Text style={{ fontSize: 14, color: Constants.Colors.Black }} numberOfLines={2}>{item.product_name}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{ fontSize: 12, color: Constants.Colors.DarkGrey, textDecorationLine: 'line-through', marginRight: 10 }}>{'₹ ' + item.price}</Text>
                                                    <Text style={{ fontSize: 12, color: Constants.Colors.HeaderGreen }}>{'₹ ' + item.discounted_price}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        {item.available ?
                                            null
                                            :
                                            <Text style={[styles.productBox, { color: Constants.Colors.White, backgroundColor: Constants.Colors.Orange, alignSelf: 'center' }]}>Not Available</Text>
                                        }
                                    </TouchableOpacity>
                                )}
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        }
                    </ScrollView>
                </Background>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerSafe: {
        flex: 1,
        backgroundColor: Constants.Colors.HeaderGreen
    },
    container: {
        flex: 1,
        //backgroundColor: Constants.Colors.HeaderGreen
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
    button: {
        margin: 3,
        width: 10,
        height: 10,
        opacity: 0.9,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Constants.Colors.LightGrey,
        borderRadius: 10 / 2
    },
    buttonSelected: {
        opacity: 1,
        color: Constants.Colors.HeaderGreen,
        height: 12,
        width: 12,
        backgroundColor: Constants.Colors.HeaderGreen,
        borderRadius: 12 / 2
    },
    customSlide: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 32,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 4,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3,
    },
    customImage: {
        width: width - 20,
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 32,
        marginTop: 10,
        borderRadius: 20,
    },
    buttons: {
        zIndex: 1,
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textInputStyle: {
        height: 45,
        width: width - 20,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        fontSize: 16,
        alignSelf: 'center',
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 4,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3,
        position: 'absolute',
        top: 65
    },
})