import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';

const { width } = Dimensions.get('window')

export default class PrivacyPolicy extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            aboutData: []
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/privacy_policy", {}, tokenValue).then((result) => {
                if (result.success == "1") {
                    this.setState({ aboutData: result.message })
                } else {
                    //alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        {this.state.aboutData && this.state.aboutData.length > 0 && this.state.aboutData.map((data, key) => {
                            return (
                                <View style={{margin: 15,}}>
                                    <Text style={{ fontSize: 18, color: Constants.Colors.Black }}>{data.title}</Text>
                                    <Text style={{ fontSize: 16, color: Constants.Colors.Gray }}>{data.description}</Text>
                                </View>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})