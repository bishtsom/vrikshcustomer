import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../utilities/RestClient';
import { Rating, AirbnbRating } from 'react-native-ratings';

const { width } = Dimensions.get('window')

export default class PreviousOrders extends Component {
    constructor(props) {
        super()
        this.state = {
            tokenValue: '',
            previousOrdersList: [],
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    componentDidMount() {
        this.getOngoingDetails();
    }

    getOngoingDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/customer/order/previous", {}, tokenValue).then((result) => {
                console.log("ongoing new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ previousOrdersList: result.Previous_Orders })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    ratingCompleted(rating, vendorId) {
        RestClient.post("api/customer/ratings/new", { rating: rating, vendor_id: vendorId }).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                //alert(result.message)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 10 }}>

                        {this.state.previousOrdersList && this.state.previousOrdersList.length > 0 && this.state.previousOrdersList.map((data, key) => {
                            return (
                                <TouchableOpacity onPress={() => navigate('OrderDetail', { orderDetail: data.products })} key={key} style={[styles.productBox, { margin: 5, flexDirection: 'column', justifyContent: 'space-between', flex: 1 }]}>
                                    <View style={{ flexDirection: 'row', flex: 1, justifyContent:'space-between' }}>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.DarkGrey}}>{data.order_id}</Text>
                                        <Text style={{ fontSize: 18, color: Constants.Colors.HeaderGreen }}>{'₹ ' + data.total_bill}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                        <Rating
                                            ratingCount={5}
                                            imageSize={30}
                                            defaultRating={1}
                                            startingValue={data.vendor_rating}
                                            onFinishRating={(rating) => this.ratingCompleted(rating, data.vendor_id)}
                                        />
                                        <TouchableOpacity onPress={() => navigate('Complain', { orderDetail: data })}>
                                            <Text style={{ color: Constants.Colors.Orange }}>Complain</Text>
                                        </TouchableOpacity>
                                    </View>

                                </TouchableOpacity>
                            )
                        })}

                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        // alignSelf: 'center',
        //alignItems: "center",
        height: 90,
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})