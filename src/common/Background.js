import React, { Component } from 'react';
import {  Platform,  StyleSheet,  Text,  View,  ImageBackground} from 'react-native';
import Constants from "../utilities/Constants";

export default class Background extends Component {
  render() {
    return (
      <View 
        style={[styles.container, this.props.style]}>
          {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex  : 1,
    width : Constants.BaseStyle.DEVICE_WIDTH,
    // backgroundColor:"rgb(53,110,174)"
    //  backgroundColor: 'rgb(0, 204, 68)',
     backgroundColor: 'white'
  },
});
