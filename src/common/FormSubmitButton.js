import React, { PropTypes } from "react";
import { TouchableOpacity, Text, StyleSheet, Dimensions, ImageBackground, View } from "react-native";
import Constants from "../utilities/Constants";

const FormSubmitButton = props => {
  const { text, onPress, style, textStyle, backGroundStyle } = props;

  return (
    <TouchableOpacity
      style={[styles.loginButtonStyle, style]}
      onPress={onPress}
    >
      <View style={[{borderRadius: 15, backgroundColor:Constants.Colors.HeaderGreen, width: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 80, padding: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3 }, backGroundStyle]}>
        <Text style={[{ color: Constants.Colors.White, textAlign: "center", fontWeight: '500', fontSize: 18 }, textStyle]}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  loginButtonStyle: {
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 5,
    marginBottom: 0,
    alignSelf:'center',
    marginHorizontal: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 10,
    borderRadius: 15,
  }
});

export default FormSubmitButton;
