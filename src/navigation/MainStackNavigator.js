import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator, createAppContainer, } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import Constants from "../utilities/Constants";
import SplashScreen from '../components/SplashScreen'
import TutorialScreen from '../components/TutorialScreen'
import Login from '../components/Login'
import SignUp from '../components/SignUp'
import Home from '../components/Home'
import Favourites from '../components/Favourites'
import About from '../components/About'
import PrivacyPolicy from '../components/PrivacyPolicy'
import OngoingOrders from '../components/OngoingOrders'

import CurrentOrders from '../components/CurrentOrders'
import OrderDetail from '../components/OrderDetail'
import Complain from '../components/Complain'

import PreviousOrders from '../components/PreviousOrders'
import MyCart from '../components/MyCart'
import AllProducts from '../components/AllProducts'
import AllCategories from '../components/AllCategories'
import ProductList from '../components/ProductList'
import Feedback from '../components/Feedback'
import PlaceOrder from '../components/PlaceOrder'
import Faq from '../components/Faq'
import Promocode from '../components/Promocode'
import DeliveryAddress from '../components/DeliveryAddress'

import ProductDetails from '../components/ProductDetails'

import ProfileDrawer from '../components/ProfileDrawer'
import MyProfile from '../components/MyProfile';

const { height, width } = Dimensions.get('window');

const HomeStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  TutorialScreen: {
    screen: TutorialScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      headerStyle: {
        backgroundColor: Constants.Colors.HeaderGreen,
        elevation: 20,
        shadowOpacity: 10
      },
      headerTintColor: '#333333',
      headerTitleStyle: {
        color: Constants.Colors.White
      }
    }
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      headerStyle: {
        backgroundColor: Constants.Colors.HeaderGreen,
        elevation: 20,
        shadowOpacity: 10
      },
      headerTintColor: '#333333',
      headerTitleStyle: {
        color: Constants.Colors.White
      }
    }
  },
  Home: {
    screen: createDrawerNavigator({
      Home: {
        screen: createStackNavigator({
          Home: {
            screen: Home,
            navigationOptions: {
              headerShown: false,
              gestureEnabled: false
            }
          },
          MyProfile: {
            screen: MyProfile,
            navigationOptions: {
              headerShown: false
            }
          },
          About: {
            screen: About,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          PrivacyPolicy: {
            screen: PrivacyPolicy,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          Favourites: {
            screen: Favourites,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          OngoingOrders: {
            screen: OngoingOrders,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          CurrentOrders: {
            screen: CurrentOrders,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          OrderDetail: {
            screen: OrderDetail,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          Complain: {
            screen: Complain,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          PreviousOrders: {
            screen: PreviousOrders,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          MyCart: {
            screen: MyCart,
            navigationOptions: {
              headerShown: false
            }
          },
          ProductDetails: {
            screen: ProductDetails,
            navigationOptions: {
              headerShown: false
            }
          },
          AllProducts: {
            screen: AllProducts,
            navigationOptions: {
              headerShown: false
            }
          },
          AllCategories: {
            screen: AllCategories,
            navigationOptions: {
              headerShown: false
            }
          }
          ,
          ProductList: {
            screen: ProductList,
            navigationOptions: {
              headerShown: false
            }
          },
          Feedback: {
            screen: Feedback,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          PlaceOrder: {
            screen: PlaceOrder,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          Faq: {
            screen: Faq,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          Promocode: {
            screen: Promocode,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
          DeliveryAddress: {
            screen: DeliveryAddress,
            navigationOptions: {
              headerStyle: {
                backgroundColor: Constants.Colors.HeaderGreen,
                elevation: 20,
                shadowOpacity: 10
              },
              headerTintColor: '#333333',
              headerTitleStyle: {
                color: Constants.Colors.White
              }
            }
          },
        })
      }
    },
      {
        contentComponent: ProfileDrawer,
        navigationOptions: {
          headerShown: false,
          gestureEnabled: false,
        },
        gestureEnabled: false,
        drawerWidth: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 75,
      }
    )
  },
},
  {
    initialRouteName: "SplashScreen",
    navigationOptions: {
      headerShown: false,
      gestureEnabled: false
    }
  }
)

const MySwitchNavigator = createSwitchNavigator({
  Home: HomeStack,
  initialRouteName: HomeStack
});

export default createAppContainer(MySwitchNavigator);