import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, TouchableOpacity, Image, Switch } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Constants from '../utilities/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions, StackActions } from "react-navigation"

var goBack = null;
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' })
    ], key: null
});

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    openCart() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            if (tokenValue == null || tokenValue == '') {
                alert("Please Sigin to the application.")
            } else {
                this.props.navigation.navigate('MyCart')
            }
        })
    }

    render() {
        const { navigate } = this.props.navigation
        goBack = () => this.props.navigation.goBack(null);
        return (
            <View style={styles.navigationBar}>
                {!this.props.goBackIcon ?
                    <TouchableOpacity onPress={this.props.onDrawerOpen} style={[styles.navIcons]}>
                        <Image source={Constants.Images.menu} style={[styles.navIcons]} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={() => this.props.homeReload ? this.props.navigation.dispatch(resetActionLogin) : goBack()} style={[styles.navIcons]}>
                        <Icon name="chevron-left" size={25} color={Constants.Colors.White} style={[styles.navIcons]} />
                    </TouchableOpacity>
                }
                <View style={{ flex: 1, alignContent: 'center', alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={[styles.HeaderTextStyle]}>{this.props.headerText}</Text>
                </View>

                <View style={styles.navBarRight}>
                    <TouchableOpacity style={{ position: 'relative' }} onPress={() => this.openCart()}>
                        <Icon name="shopping-cart" size={30} color="white" />
                        {Constants.cartCount == 0 ?
                            null
                            :
                            <View style={{ position: 'absolute', top: 15, left: 20, backgroundColor: Constants.Colors.White, borderRadius: 20 / 2, height: 20, width: 20, justifyContent: 'center', alignSelf: 'flex-end' }}>
                                <Text style={{ color: Constants.Colors.Black, alignSelf: 'center', textAlign: 'center' }}>{Constants.cartCount}</Text>
                            </View>
                        }
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Constants.BaseStyle.DEVICE_WIDTH
    },
    navigationBarcontainer: {
        width: Constants.BaseStyle.DEVICE_WIDTH,
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 8,
    },
    navigationBar: {
        backgroundColor: Constants.Colors.HeaderGreen,//Constants.Colors.LightBlue,
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 9,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30
    },
    navBarRight: {
        flexDirection: 'row',
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 7,
        //marginTop:0,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'transparent',
        marginRight: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 5,
    },
    rightButtonNav: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: (Constants.BaseStyle.DEVICE_WIDTH / 100) * 5,
    },

    navIconsGoBack: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 4,
        marginTop: 3.5,
        //marginVertical: Constants.BaseStyle.DEVICE_WIDTH*1/100,
    },
    navIcons: {
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 9,
        alignSelf: 'center',
        marginLeft: 5,
        marginTop: 2,
        borderRadius: (Constants.BaseStyle.DEVICE_WIDTH / 100 * 13) / 2
        //marginVertical: Constants.BaseStyle.DEVICE_WIDTH*1/100,
    },
    settingIcon: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 7,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 7,
        marginTop: 3.5,
        //marginVertical: Constants.BaseStyle.DEVICE_WIDTH*1/100,
    },
    HeaderTextStyle: {
        fontSize: 20,
        color: '#FFFFFF',
        alignSelf: "center",
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
});
