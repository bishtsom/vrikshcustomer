"use strict"

let colors = {
    /* App Colors Codes */
    Orange: "#f16722",
    DarkGrey: "#3f3f3f",
    LightGrey: "#adadad",
    messageTextGray: '#8EA2B1',
    //SkyBlue: '#52C7E5',
    SkyBlue: '#21b8de',
    DarkBlue: '#1e4281',
    MidBlue: '#325087',
    White: "#ffffff",
    LightBlue: "#396cb3",
    veryLightGreen: "#CBE8D1",
    checkBoxGreen: "#00B578",
    /* --------------- */
    Transparent: 'transparent',
    WhiteBlur: "rgba(235,235,235,0.7)",
    WhiteSmoke: "#f4f5f7",
    WhiteUpd: "rgb(244,248,254)",
    Snow: "#f2f3f6",
    GhostWhite: "#ebebeb",
    Gray: "#737373",
    BlurGrey: "rgba(115,115,115,0.4)",
    Blue: '#396cb3',
    //LightBlue: '#53C8E5',
    //LightBlue: '#1E4281',
    LightGray: "#a5a6a6",
    SettingLightGray: "#f2f2f2",
    Black: '#212123',
    PureBlack: "#000000",
    LightBlack: "#575758",
    Green: '#009a0e',
    LightGreen: "#7cce6e",
    HeaderGreen: "#39ac73",
    HeaderLightGreen: "#2cba14",
    Magenta: "#ef3881",
    Purple: "#a2a1b8",
    LightBrown: "#938d8a",
    SmokeWhite: "#F5F5F5",
    newOrange: "#f16722",
    newRed: "#f91722",
    BackgroundBlue: "rgb(53,110,174)",
    newBlue: "#46CBEC",
    silverGray: '#dbdbdb',
    LinearGradientBlue: '#3378c4',
    LinearGradientDarkBlue: "#042951",
    ListLightBlue: '#6b9cd1',
    ListPlaceHolderBlue: '#A9D6F3'
    //DarkBlue: "rgb(27,68,125)"
};

module.exports = colors;