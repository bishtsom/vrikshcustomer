import React from 'react'
import HomeStack from '../UserApp/src/navigation/MainStackNavigator'
//import firebase from 'react-native-firebase';
// import HuaweiProtectedApps from 'react-native-huawei-protected-apps';
// const config = {
//   title: "Huawei Protected Apps",
//   text: "This app requires to be enabled in 'Protected Apps' in order to receive push notifcations",
//   doNotShowAgainText: "Do not show again",
//   positiveText: "PROTECTED APPS",
//   negativeText: "CANCEL"
// };



const App = () => {
  console.disableYellowBox = true;
  //LogBox.ignoreAllLogs(value)
  return <HomeStack />
};

// HuaweiProtectedApps.AlertIfHuaweiDevice(config);

export default App;